# README #

This a simple Python script to integrate density cube files of the Gaussian98 format.
This started out from a need for one of my own projects, and has grown in to a tool 
with a lot more functionality. There is likely some overlap with CubeCruncher, but 
I've never used it extensively to check. I make this implementation available 
to anyone who might find it useful. 

* v1.0
* To do: 
    * Automated testing 
    * Performance improvements
    * Line profiles that do not lie along a crystal axis.

### Features ###

Reads a Gaussian cube file and can carry out one of three operations:

* D3 - Takes the cube and returns a four-column array of the voxels' coordinates
       and values
* D2 - Takes the cube, sums over one axis, and reutrns the results as a three
       column array.
* D1 - Takes the cube, sums over two axes, and reutrns the results as a two 
       column array.

For all modes the data points can be returned raw, or multiplied by the volume of 
a voxel. This is sometimes necessary depending on how the code which generated the 
cube file wrote the data. An example might be a cube file which shows electron 
density. This should integrate to the total number of electrons in the simulation
box. However, the data may be in units of electrons, or electrons/vol. In this 
script, I provide a keyword to handle this conversion. 

The total of all data points, and the total of all data points over each axis,
is printed at the top of the output file for every mode. This can be useful 
for determining which units have been used for a given cube file.

For the D2 and D1 modes, there is also the option to average over the axis which
has been integrated out. This is useful for generating contour plots or line 
profiles in 'real' units.

### Setup ###

The script requires Python 2.7+ to be installed and Numpy.
The script does not need installed and in principle should work immediately. 

### Basic Usage ###

Use with:
    `./CubeIntegrator.py  <MODE> <CUBE FILE>  [-h] [-o]`
  
    Arguments
    ------------
    Required: 
        <MODE>       - Which operation to carry out on the cube file.
                       Permitted values : D3 , D2 , D1
                       Mode D3 returns the cube file formatted into 3 columns
                       Mode D2 integrates the cube over one dimension.
                       Mode D1 integrates the cube over two dimension.
        <CUBE FILE>  - Path to a Guassian98 formatted cube file
    
    Optional:
        [-o],[--output] - A string to name the output data (opt)
        [-h],[--help]   - Print the help text 
    
    Returns
    -----------
    For the 3D case (no integration done): 
        A four column file with columns - x y z value
    For the 2D case (integration over x,y,or z): 
        A three column file - two coordinates and values
    For the 1D case (integration over two axes): 
        A two column file - one coordinate and values




**Copyright (c) 2017 Conrad Johnston - All rights reserved.**  
**Use at your own risk! No warranty provided.**  
**Contact: conrad DOT s DOT johnston AT googlemail DOT com**
