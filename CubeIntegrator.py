#!/usr/bin/env python
"""
-------------------------------------------------------------------------------
  Simple script to carry out operations on Gaussian98 density cube files.
  
  Reads in one Guassian98 cube file, and reformats the cube as a 
  printed 4 column array, or integrates over either one or two dimensions.
  
  Usage: CubeIntegrator.py  <MODE> <CUBE FILE>  [-h] [-o]  
  
  Arguments
  ------------
  Required: 
      <MODE>       - Which operation to carry out on the cube file.
                     Permitted values : D3 , D2 , D1
                     Mode D3 returns the cube file formatted into 3 columns
                     Mode D2 integrates the cube over one dimension.
                     Mode D1 integrates the cube over two dimension.
      <CUBE FILE>  - Path to a Guassian98 formatted cube file
  
  Optional:
      [-o],[--output]  - A string to name the output data (opt)
      [-h],[--help]    - Print the help text 
  
  Returns
  -----------
  For the 3D case (no integration done): 
      A four column file with columns - x y z value
  For the 2D case (integration over x,y,or z): 
      A three column file - two coordinates and values
  For the 1D case (integration over two axes): 
      A two column file - one coordinate and values
  
  -----------
  Copyright (c) 2017 Conrad Johnston - All rights reserved 
  Use at your own risk! No warranty provided.
  Contact: conrad [dot] s [dot] johnston [at] googlemail [dot] com
------------------------------------------------------------------------------
"""

import numpy as np
import argparse as ap
#from subprocess import call
import sys 
import textwrap

# =============================================================================
# Input Parser
# =============================================================================

class CubeInt:
     
    def __init__(self):
         
        main_parser = CustomParser(
                usage="CubeIntegrator.py  <MODE> <CUBE FILE>  [-h] [-o]",
                formatter_class=ap.RawTextHelpFormatter,
                add_help=False)

        # Main Positional Arguments
        reqgroup = main_parser.add_argument_group("Required Arguments")
        
        reqgroup.add_argument(
            'MODE',
            type=str,
            help="Operation to carry out - either 'D3', 'D2', or 'D1'")
   
        # General Optional Arguments
        optgroup = main_parser.add_argument_group("Options")
        
        optgroup.add_argument(
            '-h','--help',
            action='help',
            help="Show this message and exit")
        
        # Parse
        args = main_parser.parse_args(sys.argv[1:2])
        self.mode = args.MODE

        ## Invoke correct method   
        if not hasattr(self, args.MODE):
            print("\nError: Mode not recognised. Allowed values are 'D3', 'D2', or 'D1'.\n")
            sys.exit(1)
        else:
            getattr(self, args.MODE)()

# =============================================================================
# Bulk 
# =============================================================================

# -----------------------------------------------------------------------------
# D3
# -----------------------------------------------------------------------------
    def D3(self):
        """
        -----------------------------------------------------------------------
         Reads in one Guassian98 cube file, and reformats the cube as a 
         printed 4 column array.
          
         Usage: CubeIntegrator.py  D3 <CUBE FILE>  [-h] [-o] [-v] 
         
         Arguments
         ------------
         Required: 
             <CUBE FILE>  - Path to a Guassian98 formatted cube file
         
         Optional:
             [-h],[--help]     - Print the help text 
             [-o],[--output]   - A string to name the output data (opt)
             [-v],[--volunits] - If 'no', convert the units from density
                                 to per voxel values. (DEFAULT=yes).

         Returns
         -----------
         A four column file with columns - x y z value
          
        -----------------------------------------------------------------------

        """
        
        # -- SUB-PARSE OPTIONS ------------------------------------------------
        
        # Invoke parser
        sub_parser = CustomParser(
            mode=getattr(self,'D3'), 
            usage='''CubeIntegrator.py D3 <CUBE FILE> [-o][-v][-h]
            ''',
            formatter_class=ap.RawTextHelpFormatter,
            add_help=False)
    
        # Main Positional Arguments
        reqgroup = sub_parser.add_argument_group("Required Arguments")
        
        reqgroup.add_argument(
            'CUBE',
            help="A cube file to process") 
       
        # General Optional Arguments
        optgroup = sub_parser.add_argument_group("Options")

        optgroup.add_argument(
            '-h', '--help',
            action="help",
            help="Show this help message and exit")
       
        optgroup.add_argument(
            '-o', '--output', 
            default="ConvertedCUBE.dat",
            help="Specify a name for the output file")
        
        optgroup.add_argument(
            '-v', '--volunits', default='yes',
            choices=['yes','no'],
            help="Output the units as in terms of density, as per the cube file.")

        args = sub_parser.parse_args(sys.argv[2:])
        
        # -- PROCEDURE -------------------------------------------------------
        
        # Read the cube file
        print("\n Reading the cube file...")
        headerdata,voxel,dims,Vol3Ddata = parse_cube(args.CUBE)
        print("\033[F Reading the cube file... DONE")
        
        # Print some data about the cube file
        print("\n Voxel array:")
        print( " {: .4f} {: .4f} {: .4f}".format(voxel[0,0], voxel[0,1], voxel[0,2]))
        print( " {: .4f} {: .4f} {: .4f}".format(voxel[1,0], voxel[1,1], voxel[1,2]))
        print( " {: .4f} {: .4f} {: .4f}".format(voxel[2,0], voxel[2,1], voxel[2,2]))

        voxelvol=np.linalg.det(voxel)    
        print("\n Volume of voxel:")
        print( "  {: .4f}".format(voxelvol))
        
        print("\n Number of voxels (x, y, z):")
        print(" {: .0f} {: .0f} {: .0f}".format(dims[0], dims[1], dims[2]))
        
        if args.volunits=='no':
            # Convert from volume to real units 
            print("\n Converting from density units...")
            Vol3Ddata=Vol3Ddata*voxelvol
            print("\033[F Converting from density units...DONE")

        # Write the cube file as a four-column ASCII file
        print("\n Writing to the output file...")
        write_ascii(args.output,headerdata,voxel,dims,Vol3Ddata,"","",False)
        print("\033[F Writing to the output file... DONE\n")

# -----------------------------------------------------------------------------
# D2
# -----------------------------------------------------------------------------
    def D2(self): 
        """
        -----------------------------------------------------------------------
         Reads in one Guassian98 cube file, integrates over one axis and 
         formats the output as a 3 column array, 
          
         Usage: CubeIntegrator.py  D2 <AXIS> <CUBE FILE>  [-h] [-o] [-v] [-a] [-g]
         
         Arguments
         ------------
         Required:
             <AXIS>       - The axis over which to integrate
             <CUBE FILE>  - Path to a Guassian98 formatted cube file
         
         Optional:
             [-h],[--help]     - Print the help text 
             [-o],[--output]   - A string to name the output data (opt)
             [-v],[--volunits] - If 'no', convert the units from density
                                 to per voxel values. (DEFAULT=yes).
             [-a],[--average]  - After summing over an axis, calculate the 
                                 average for the resulting values (DEFAULT=no) 
             [-g],[--gnuplot] - Adjusts the output formatting for the Gnuplot pm3d plot type
         
         Returns
         -----------
         A three column file with columns - axis1 axis2 value
          
        -----------------------------------------------------------------------

        """
        
        # -- SUB-PARSE OPTIONS ------------------------------------------------

        # Invoke parser
        sub_parser = CustomParser(
            mode=getattr(self, 'D2'),
            usage='''CubeIntegrator.py D2 <AXIS> <CUBE FILE> [-h][-o][-v][-a]
            ''',
            formatter_class=ap.RawTextHelpFormatter,
            add_help=False)
    
        # Main Positional Arguments
        reqgroup = sub_parser.add_argument_group("Required Arguments")
        
        reqgroup.add_argument(
            'AXIS',
            choices=['a','b','c'],
            help="The axis over which to integrate (a, b or c)") 
        
        reqgroup.add_argument(
            'CUBE',
            help="A cube file to process") 
        
        # General Optional Arguments
        optgroup = sub_parser.add_argument_group("Options")

        optgroup.add_argument(
            '-h', '--help',
            action="help",
            help="Show this help message and exit")
       
        optgroup.add_argument(
            '-o', '--output', 
            default="ConvertedCUBE.dat",
            help="Specify a name for the output file")
        
        optgroup.add_argument(
            '-v', '--volunits', default='yes',
            choices=['yes','no'],
            help="Output the units as in terms of density, as per the cube file.")
        
        optgroup.add_argument(
            '-a', '--average', default='no',
            choices=['yes','no'],
            help="Average over number of elements in the integrated direction")
        
        optgroup.add_argument(
            '-g', '--gnuplot', default='no',
            choices=['yes','no'],
            help="Formats the output for the Gnuplot pm3d mode.")

        args = sub_parser.parse_args(sys.argv[2:])

        # -- PROCEDURE -------------------------------------------------------
        
        # Read the cube file
        print("\n Reading the cube file...")
        headerdata,voxel,dims,Vol3Ddata = parse_cube(args.CUBE)
        print("\033[F Reading the cube file... DONE")
        
        # Print some data about the cube file
        print("\n Voxel array:")
        print( " {: .4f} {: .4f} {: .4f}".format(voxel[0,0], voxel[0,1], voxel[0,2]))
        print( " {: .4f} {: .4f} {: .4f}".format(voxel[1,0], voxel[1,1], voxel[1,2]))
        print( " {: .4f} {: .4f} {: .4f}".format(voxel[2,0], voxel[2,1], voxel[2,2]))

        voxelvol=np.linalg.det(voxel)    
        print("\n Volume of voxel:")
        print( "  {: .4f}".format(voxelvol))
        
        print("\n Number of voxels (x, y, z):")
        print(" {: .0f} {: .0f} {: .0f}".format(dims[0], dims[1], dims[2]))

        # Integrate over the specified dimension 
        print("\n Integrating over \'{:s}\'...".format(args.AXIS))
        Vol3Ddata=integrate1D(args.AXIS,Vol3Ddata)
        print("\033[F Integrating over \'{:s}\'...DONE".format(args.AXIS))

        if args.average=='yes':
           # Average over the specified dimension 
           print("\n  Averaging over the elements of \'{:s}\'...".format(args.AXIS))
           Vol3Ddata=average1D(args.AXIS,dims,Vol3Ddata)
           print("\033[F Averaging over the elements of \'{:s}\'...DONE".format(args.AXIS))
           
        if args.volunits=='no':
            # Convert from volume to real units 
            print("\n Converting from density units...")
            Vol3Ddata=Vol3Ddata*voxelvol
            print("\033[F Converting from density units...DONE")

        if args.gnuplot=='yes':
            gnuplot=True
        else:
            gnuplot=False
            
        # Write the cube file as a four-column ASCII file
        print("\n Writing to the output file...")
        write_ascii(args.output,headerdata,voxel,dims,Vol3Ddata,args.AXIS,'',gnuplot)
        print("\033[F Writing to the output file... DONE\n")

# -----------------------------------------------------------------------------
# D1
# -----------------------------------------------------------------------------
    def D1(self): 
        """
        -----------------------------------------------------------------------
         Reads in one Guassian98 cube file, integrates over one axis and 
         formats the output as a 2 column array, 
          
         Usage: CubeIntegrator.py  D1 <AXIS1> <AXIS2> <CUBE FILE>  [-h] [-o] [-v] [-a]
         
         Arguments
         ------------
         Required:
             <AXIS1>      - The first axis over which to integrate
             <AXIS2>      - The second axis over which to integrate
             <CUBE FILE>  - Path to a Guassian98 formatted cube file
         
         Optional:
             [-h],[--help]     - Print the help text 
             [-o],[--output]   - A string to name the output data (opt)
             [-v],[--volunits] - If 'no', convert the units from density
                                 to per voxel values. (DEFAULT=yes).
             [-a],[--average]  - After summing over the two axes, calculate the 
                                 average for the resulting values 
         
         Returns
         -----------
         A two column file with columns - axis value
          
        -----------------------------------------------------------------------

        """
        
        # -- SUB-PARSE OPTIONS ------------------------------------------------

        # Invoke parser
        sub_parser = CustomParser(
            mode=getattr(self, 'D1'),
            usage='''CubeIntegrator.py D1 <AXIS1> <AXIS2> <CUBE FILE> [-h][-o][-v][-a]
            ''',
            formatter_class=ap.RawTextHelpFormatter,
            add_help=False)
        
        # Main Positional Arguments
        reqgroup = sub_parser.add_argument_group("Required Arguments")
        
        reqgroup.add_argument(
            'AXIS1',
            choices=['a','b','c'],
            help="The first axis over which to integrate (a, b or c)") 
        
        reqgroup.add_argument(
            'AXIS2',
            choices=['a','b','c'],
            help="The second axis over which to integrate (a, b or c)") 
        
        reqgroup.add_argument(
            'CUBE',
            help="A cube file to process") 
        
        # General Optional Arguments
        optgroup = sub_parser.add_argument_group("Options")

        optgroup.add_argument(
            '-h', '--help',
            action='help',
            help="Show this help message and exit")
       
        optgroup.add_argument(
            '-o', '--output', 
            default="ConvertedCUBE.dat",
            help="Specify a name for the output file")
        
        optgroup.add_argument(
            '-a', '--average', default='no',
            choices=['yes','no'],
            help="Average over number of elements in the integrated direction")
        
        optgroup.add_argument(
            '-v', '--volunits', default='yes',
            choices=['yes','no'],
            help="Output the units as in terms of density, as per the cube file.")

        args = sub_parser.parse_args(sys.argv[2:])

        
        # -- PROCEDURE -------------------------------------------------------
       
        # Check that two distinct axes have been specified
        if args.AXIS1==args.AXIS2:
            print("ERROR: AXIS1 and AXIS2 must be different")
            sys.exit(1)

        # Sort the axis for convenience
        if args.AXIS1=='b':
            if args.AXIS2=='a':
                args.AXIS1='a'; args.AXIS2='b'
        elif args.AXIS1=='c':
            if args.AXIS2=='a':
                args.AXIS1='a'; args.AXIS2='c'
            if args.AXIS2=='b':
                args.AXIS1='a'; args.AXIS2='b'

        # Read the cube file
        print("\n Reading the cube file...")
        headerdata,voxel,dims,Vol3Ddata = parse_cube(args.CUBE)
        print("\033[F Reading the cube file... DONE")
        
        # Print some data about the cube file
        print("\n Voxel array:")
        print( " {: .4f} {: .4f} {: .4f}".format(voxel[0,0], voxel[0,1], voxel[0,2]))
        print( " {: .4f} {: .4f} {: .4f}".format(voxel[1,0], voxel[1,1], voxel[1,2]))
        print( " {: .4f} {: .4f} {: .4f}".format(voxel[2,0], voxel[2,1], voxel[2,2]))

        voxelvol=np.linalg.det(voxel)    
        print("\n Volume of voxel:")
        print( "  {: .4f}".format(voxelvol))
        
        print("\n Number of voxels (x, y, z):")
        print(" {: .0f} {: .0f} {: .0f}".format(dims[0], dims[1], dims[2]))

        # Integrate over the two specified dimension 
        print("\n Integrating over \'{:s}\' and \'{:s}\'...".format(args.AXIS1, args.AXIS2))
        Vol3Ddata=integrate2D(args.AXIS1,args.AXIS2,Vol3Ddata)
        print("\033[F Integrating over \'{:s}\' and \'{:s}\'...DONE".format(args.AXIS1,args.AXIS2))

        if args.average=='yes':
            # Average over the specified dimension 
            print("\n  Averaging over the elements of \'{:s}\' and \'{:s}\' ...".format(args.AXIS1,args.AXIS2))
            Vol3Ddata=average1D(args.AXIS1,dims,Vol3Ddata)
            Vol3Ddata=average1D(args.AXIS2,dims,Vol3Ddata)
            print("\033[F Averaging over the elements of \'{:s}\' and \'{:s}\'...DONE".format(args.AXIS1,args.AXIS2))
       
        if args.volunits=='no':
            # Convert from volume to real units 
            print("\n Converting from density units...")
            Vol3Ddata=Vol3Ddata*voxelvol
            print("\033[F Converting from density units...DONE")
            
        # Write the cube file as a two-column ASCII file
        print("\n Writing to the output file...")
        write_ascii(args.output,headerdata,voxel,dims,Vol3Ddata,args.AXIS1,args.AXIS2,False)
        print("\033[F Writing to the output file... DONE\n")

# =============================================================================
# Subroutine Definition
# =============================================================================

def parse_cube(filename):
    
    with open(filename) as f:
    
        lines = list(f)
        atoms_line = lines[2].split() # Grab the third line, splitting its contents 
        atoms = int(atoms_line[0])
        header = lines[:6+atoms]  # turn first lines in to a list
        data_lines = lines[6+atoms:] # read in rest of data lines
        
        #Get the number of data points in each dimension
        dims=np.zeros(3)     
        x_line = header[3].split() # Grab the fourth line, splitting its contents 
        dims[0] = int(x_line[0])
        y_line = header[4].split() # Grab the fifth line, splitting its contents 
        dims[1] = int(y_line[0])
        z_line = header[5].split() # Grab the sixth line, splitting its contents 
        dims[2] = int(z_line[0])

        #Get the vectors describing the voxels
        voxel=np.array([[x_line[1], x_line[2], x_line[3]],
                       [ y_line[1], y_line[2], y_line[3]],
                       [ z_line[1], z_line[2], z_line[3]]],
                       dtype='f')

        xdim=int(dims[0]); ydim=int(dims[1]); zdim=int(dims[2]);
        Vol3Ddata = np.zeros((xdim,ydim,zdim))

        datalist=[]
        for line in data_lines:
            for i in range(0, len(line), 13):
                data_point = line[i:i+13].strip()
                if data_point != "":
                    datalist.append(float(data_point))

        # Run over the x-values
        for i in range(0,xdim): 
            # Run over the y-values
            for j in range(0,ydim):
                # Run over z-values
                for k in range(0,zdim):
                    Vol3Ddata[i,j,k]=(datalist[(i*ydim*zdim)+(j*zdim)+k])

    return (header, voxel, dims, Vol3Ddata)


def write_ascii(filename, header, voxel, dims, Vol3Ddata, axis1, axis2, gnuplot): 
    # Write data out to a four column format from the numpy 3D array

    out = open(filename,'w')
    
    #Reprint the two comment lines from the cube file
    out.write('# ' + header[0])
    out.write('# ' + header[1])

    # Calculate the length of the voxel vectors
    a=np.linalg.norm(voxel[0,:])
    b=np.linalg.norm(voxel[1,:])
    c=np.linalg.norm(voxel[2,:])
    xdim=int(dims[0]); ydim=int(dims[1]); zdim=int(dims[2]);
    
    # Check the length of the data array
    datadims=len(np.shape(Vol3Ddata))
    
    #Print the datapoints
    if datadims==3:
        #Print out some totals
        out.write('# Total of data: {: 7.5E} ; a = {: 7.5E} ; b = {: 7.5E} ; \
                   c= {: 7.5E}\n'.format(Vol3Ddata.sum(),Vol3Ddata[0].sum(),Vol3Ddata[1].sum(),
                   Vol3Ddata[2].sum()))
        #Print the datapoints
        out.write('# Coords are in bohr\n')
        out.write('# a-pos b-pos c-pos Value\n')
        # Run over the x-values
        for i in range(0, xdim): 
            # Run over the y-values
            for j in range(0, ydim):
                # Run over z-values
                for k in range(0,zdim):
                    # Calculate the position in bohr 
                    xpos=i*a; ypos=j*b; zpos=k*c;
                    # Print the data
                    out.write('{: 7.5E} {: 7.5E} {: 7.5E} {: 7.5E}\n'.format(xpos,ypos,zpos,Vol3Ddata[i,j,k]))

    elif datadims==2:
        #Write out some column headers
        out.write('# Coords are in bohr\n')
        if axis1=='a':
            out.write('# Total of data: {: 7.5E} ; b = {: 7.5E} ; c= {: 7.5E}\n'.format(Vol3Ddata.sum(),
                      Vol3Ddata[0].sum(),Vol3Ddata[1].sum()))
            out.write('# b-pos c-pos Value\n')
        elif axis1=='b':
            out.write('# Total of data: {: 7.5E} ; a = {: 7.5E} ; c= {: 7.5E}\n'.format(Vol3Ddata.sum(),
                      Vol3Ddata[0].sum(),Vol3Ddata[1].sum()))
            out.write('# a-pos c-pos Value\n')
        elif axis1=='c':
            out.write('# Total of data: {: 7.5E} ; a = {: 7.5E} ; b= {: 7.5E}\n'.format(Vol3Ddata.sum(),
                      Vol3Ddata[0].sum(),Vol3Ddata[1].sum()))
            out.write('# a-pos b-pos Value\n')
        else:
            print("Axis not recognised")
            sys.exit(1)
        # Run over the x/y values
        for i in range(0, np.shape(Vol3Ddata)[0]): 
            # Run over the y/z values
            for j in range(0, np.shape(Vol3Ddata)[1]): 
                    # Calculate the position in bohr 
                    if axis1=='a':
                        xpos=i*b; ypos=j*c;
                    elif axis1=='b':
                        xpos=i*a; ypos=j*c;
                    elif axis1=='c':
                        xpos=i*a; ypos=j*b;
                    else:
                        print("Axis not recognised") 
                        sys.exit(1)
                        
                    # Print the data
                    out.write('{: 7.5E} {: 7.5E} {: 7.5E}\n'.format(xpos,ypos,Vol3Ddata[i,j]))
            if gnuplot:
                out.write('\n')

    elif datadims==1:
        #Write out some column headers
        out.write('# Coords are in bohr\n')
        out.write('# Total of data: {: 7.5E}\n'.format(Vol3Ddata.sum()))
        if axis1=='a':
            if axis2=='b':
                out.write('# c-pos Value\n')
            elif axis2=='c':
                out.write('# b-pos Value\n')
            else:
                print("Axis not recognised")
                sys.exit(1)
        # Only BC is now possible. 
        elif axis1=='b':
            out.write('# a-pos Value\n')
        else:
            print("Axis not recognised")
            sys.exit(1)
        #Run over the x/y/z values
        for i in range(0, np.shape(Vol3Ddata)[0]): 
            # Calculate the position in bohr 
            # There are 3 unique planes - check which it is
            if axis1=='a':
                if axis2=='b':
                    xpos=i*c;
                elif axis2=='c':
                    xpos=i*b;
                else:
                    print("Axis not recognised")
                    sys.exit(1)
            # Only BC is now possible. 
            elif axis1=='b':
                xpos=i*a;
            else:
                print("Axis not recognised")
                sys.exit(1)
                
            # Print the data
            out.write('{: 7.5E} {: 7.5E}\n'.format(xpos,Vol3Ddata[i]))
    return

def write_cube(filename, header, voxel, dims, Vol3Ddata):
    # Write data out to the gaussian cube format from the numpy 3D array
    out = open(filename,'w')
    
    #Reprint the two comment lines from the cube file
    for ii in header:
        out.write(ii)

    xdim=int(dims[0]); ydim=int(dims[1]); zdim=int(dims[2]);
    # Run over the x-values
    for i in range(0, xdim): 
        # Run over the y-values
        for j in range(0, ydim):
            # Run over z-values
            # Run over rows that will be outputted as full rows.
            for k in range(0,((zdim-zdim%6)/6)):   # Python iterators run UP TO range
                # Deal with filled rows 
                for m in range(0,6):    
                    out.write("{: 7.5E} ".format(Vol3Ddata[i,j,((6*k)+m)])),
                out.write("\n")
            # Deal with the partially filled rows
            for m2 in range (0,zdim%6):
                out.write("{: 7.5E} ".format(Vol3Ddata[i,j,((zdim-zdim%6)+m2)])),
            out.write("\n")
    return

def integrate1D(axs, Vol3Ddata):
    # Parse the axis code
    if axs == 'a':
        axisindex=0
    elif axs == 'b':
        axisindex=1
    elif axs == 'c':
        axisindex=2
    # Integrate over a single axis
    intdata=np.sum(Vol3Ddata,axis=axisindex)
    return intdata

def integrate2D(axs1, axs2, Vol3Ddata):
    # Parse the axis1 and axis2 codes
    if axs1 == 'a':
        axis1index=0
        if axs2 == 'b':
            axis2index=0
        elif axs2 == 'c':
            axis2index=1
        else:
            print("Invalid second axis specified")
            sys.exit(1)
    
    elif axs1 == 'b':
        axis1index=1
        if axs2 == 'a':
            axis2index=0
        elif axs2 == 'c':
            axis2index=1
        else:
            print("Invalid second axis specified")
            sys.exit(1)
    
    elif axs1 == 'c':
        axis1index=2
        if axs2 == 'a':
            axis2index=0
        elif axs2 == 'b':
            axis2index=1
        else:
            print("Invalid second axis specified")
            sys.exit(1)
    
    # Integrate over the first axis
    intdata=np.sum(Vol3Ddata,axis=axis1index)
    # Integrate over the second axis
    intdata=np.sum(intdata,axis=axis2index)
    
    return intdata

def average1D(axs, dims, Vol3Ddata):
    # Parse the axis code
    if axs == 'a':
        axisindex=0
    elif axs == 'b':
        axisindex=1
    elif axs == 'c':
        axisindex=2
    # Integrate over a single axis
    intdata=Vol3Ddata/dims[axisindex]
    return intdata

def FormatHelp(self):
        formatter = self._get_formatter() #by default, an instance of argparse.HelpFormatter

        # usage
        formatter.add_usage(self.usage, self._actions,
                            self._mutually_exclusive_groups)

        # description
        formatter.add_text(self.description)

        # positionals, optionals and user-defined groups
        for action_group in self._action_groups:
            formatter.start_section(action_group.title)
            formatter.add_text(action_group.description)
            formatter.add_arguments(action_group._group_actions)
            formatter.end_section()

        # epilog
        formatter.add_text(self.epilog)

        # determine help from format above
        return formatter.FormatHelp()



class CustomParser(ap.ArgumentParser):
    '''
    This class is added so that the normal argparse "format help" 
    method can be overridden. I don't like the formatting of the normal 
    help so I just want to print the mode's doc string, but whilst 
    keeping all the help system's functionality. 

    To init I've also added the 'mode' attribute so that the correct
    doc string can be called. 
    '''
    
    def __init__(self, mode='CubeInt', *args, **kwargs):
        self.mode = mode
        super(CustomParser, self).__init__(*args, **kwargs)

    def format_help(self):
        if self.mode == 'CubeInt':
            return __doc__
        else:
            return textwrap.dedent(self.mode.__doc__)
        

# ============================================================================

CubeInt()
